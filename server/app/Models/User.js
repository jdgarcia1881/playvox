'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class User extends Model {
    satisfaction() {
        return this.belongsTo('App/Models/Satisfaction')
    }
    prioritie() {
        return this.belongsTo('App/Models/Prioritie')
    }
    statu() {
        return this.belongsTo('App/Models/Statu')
    }
}

module.exports = User
