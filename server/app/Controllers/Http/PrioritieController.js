'use strict'
const Prioritie = use('App/Models/Prioritie')

class PrioritieController {
    async index({ response }) {
        let prioritie = await Prioritie.all()
        return response.json(prioritie)
    }
}

module.exports = PrioritieController
