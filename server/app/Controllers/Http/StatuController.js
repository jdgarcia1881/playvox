'use strict'
const Statu = use('App/Models/Statu')

class StatuController {
    async index({ response }) {
        let statu = await Statu.all()
        return response.json(statu)
    }
}

module.exports = StatuController
