'use strict'
const User = use('App/Models/User');

class UserController {
    async index({ response }) {
        const users = await User.query()
        .with('satisfaction')
        .with('prioritie')
        .with('statu')
        .fetch()
        return response.json(users)
    }

    async register({ request, response }) {
        const userInfo = request.only(['name', 'subject', 'satisfaction_id', 'prioritie_id',
        'statu_id'])
        await User.create(userInfo);
        
        return response.send({ message: 'User has been created' })
    }
    async show({params, response}) {
        const user = await User.find(params.id);
        return response.json(user);
    }
}

module.exports = UserController
