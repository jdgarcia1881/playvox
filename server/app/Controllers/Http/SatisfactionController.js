'use strict'
const Satisfaction = use('App/Models/Satisfaction')

class SatisfactionController {
    async index({ response }) {
        let satisfaction = await Satisfaction.all()
        return response.json(satisfaction)
    }
}

module.exports = SatisfactionController
